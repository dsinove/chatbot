var $form,
    name,
    init = true,
    conv = null,
    $audio;

$(function () {

    conv = new Vue({
        el: "#chat-conv",
        data: {
            chats: [],
            reply: false
        }
    });

    $("form#contact").submit(function () {
        var data = $(this).serializeArray(),
            fields = {};

        for (var i in data) {
            fields[data[i].name] = data[i].value;

            if (data[i].name === "nome"){
                name = data[i].value;
            }
        }

        send(fields);

        $("body").removeClass('init');

        return false;
    })
        .find("[name=\"telefone\"]").mask("99999999999");

    $(".volume-audio").click(function () {

        if (!$(this).hasClass('off')) {
            $audio[0].pause();
        }else{
            $audio[0].play();
        }

        $(this).toggleClass('off');
    });

    formChat();
});

function audioPlay(audio) {
    var $btn = $(".volume-audio"),
        off = $btn.hasClass('off'),
        audio_id = "audio-play";

    if (audio && !off) {
        $audio = $('#' + audio_id);

        if ($audio.length > 0) {
            $audio.remove();
        }

        $("body").append("<audio id=\"" + audio_id + "\" autoplay><source src=\"" + audio + "\" type=\"audio/mpeg\"></audio>");

        $audio = $('#' + audio_id);
    }
}

function replyScroll() {
    var $replyBlock = $(".reply-block"),
        widthTotal = 0;

    $replyBlock.find('a').each(function (i, e) {
        widthTotal += $(e).width() + 35;
    });

    $replyBlock.find(".reply-container").width(widthTotal);

}

function appendInChat(data) {
    conv.chats.push({
        name: data.name,
        me: data.me === true,
        text: data.text,
        loading: data.loading
    });

    conv.reply = data.reply;

    setTimeout(function () {
        if (data.reply) {
            replyScroll();
        }
        forceBottom();
    }, 500);

    audioPlay(data.audio);
}

var loadingTimer,
    dots = 0;

function loadind(clear) {
    var $text = $(".chats .chat.loading .loading-text b");

    if (clear && loadingTimer) {
        dots = 0;
        $text.text('');

        clearTimeout(loadingTimer);

        if (clear === 'reload') {
            loadind();
        } else {
            $text.closest('.chat.loading').remove();
        }

    } else if (dots === 3) {
        loadind('reload');
    } else {
        $text.prepend('.');

        dots++;

        loadingTimer = setTimeout(loadind, 500);
    }
}

function send(text) {
    $form.find('[type="submit"]').attr('disabled', 'disabled');

    var dataSend = {
        text: text
    };

    if (init) {
        dataSend.init = 1;

        init = false;
    }

    appendInChat({
        name: '',
        me: false,
        text: '',
        loading: true,
        reply: false
    });

    loadind();

    $.post('/', dataSend, function (data) {

        loadind(true);

        appendInChat(data);

        $form.find('[type="submit"]').removeAttr('disabled');
    });

}

function sendMessageBtn($this) {
    $form.find('input').val($($this).text());
    $form.submit();
}

function formChat() {
    $form = $("form#chat");

    $form.find('input').focus();

    $form.submit(function () {
        var $input = $(this).find('.form-control'),
            text = $input.val();

        if (text) {
            $input.val('');

            appendInChat({
                name: name,
                text: [text],
                me: true,
                reply: false
            });

            send(text);

        }


        return false;
    });
}

function forceBottom() {
    var $list = $(".chat-box"),
        height = 0;

    if ($list.find('.chats .chat').length > 5) {

        height = (parseInt($list.find('.chats').height()));
        height = height + (parseInt($("#chat-conv .panel-footer").height()));

        $list.scrollTop(height);
    }
}
