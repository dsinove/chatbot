<?php
// src/Component/Watson/MessengerApi.php
namespace App\Component\Facebook;

use Exception;

use Kerox\Messenger\Messenger;
use Kerox\Messenger\Event\MessageEvent;

use Kerox\Messenger\Api\Send;
use Kerox\Messenger\Model\Message;
use Kerox\Messenger\Model\Message\QuickReply;

/**
 * Class MessengerApi
 * @package App\Component\Facebook
 */
class MessengerApi extends Messenger
{
    /**
     * @var null|int
     */
    private $senderId = NULL;

    /** Armazena os textos
     * @var array
     */
    private $text = [];

    /** Armazena as respostas rapidas
     * @var array
     */
    private $reply = [];

    /** url do audio
     * @var null|string
     */
    private $audio = NULL;

    /** usando para limpar sessao e evitar de enviar o botão encerrar
     * @var bool
     */
    public $clear = FALSE;

    /**
     * MessengerApi constructor.
     * @param string $appSecret
     * @param string $verifyToken
     * @param string $pageToken
     * @throws Exception
     */
    public function __construct(string $appSecret, string $verifyToken, string $pageToken)
    {
        if ($appSecret && $verifyToken && $pageToken) {
            parent::__construct($appSecret, $verifyToken, $pageToken);
        } else {
            throw new Exception("Error Facebook Env");
        }
    }

    /**
     * Seta o SenderId
     * @param int $senderId
     * @return $this
     */
    public function setSenderId(int $senderId)
    {
        $this->senderId = $senderId;

        return $this;
    }

    /**
     * Seta os textos
     * @param array $text
     * @return $this
     */
    public function setText(array $text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Seta as respostas rapidas
     * @param array $reply
     * @return $this
     */
    public function setReply(array $reply)
    {
        $this->reply = $reply;

        return $this;
    }

    /**
     * Seta a url do audio
     * @param string $audio
     * @return $this
     */
    public function setAudio(string $audio)
    {
        $this->audio = $audio;

        return $this;
    }

    /**
     * Pega as messagens e eventos enviados pelo facebook
     * @throws Exception
     */
    public function getMessages()
    {
        $events = $this->webhook()->getCallbackEvents();

        $messages = [];

        foreach ($events as $event) {

            if ($event instanceof MessageEvent) {
                $message = (object)[];
                $m = $event->getMessage();

                if ($m->hasText()) {
                    $message->text = $m->getText();
                    $message->senderId = $event->getSenderId();
                    if ($event->isQuickReply()) {
                        $message->quickReply = $m->getQuickReply();
                    }

                    $messages[] = $message;
                }
            }
        }

        return $messages;
    }

    /**
     * Envia um texto com as respostas rapidas
     * @param null|string $text
     * @throws Exception
     */
    private function sendReply($text = NULL)
    {
        if ($text) {
            $message = new Message($text);

            $reply = [];
            $total = 0;

            foreach ($this->reply as $row) {
                if ($row != "_clear_") {
                    $reply[] = QuickReply::create(QuickReply::CONTENT_TYPE_TEXT)
                        ->setTitle($row)
                        ->setPayload("MESSAGE");
                    $total++;
                }
            }

            if ($total > 0) {
                $message->setQuickReplies($reply);
            }

            $message->addQuickReply(QuickReply::create(QuickReply::CONTENT_TYPE_TEXT)
                ->setTitle("Encerrar Conversa")
                ->setPayload("CLEAR")
                ->setImageUrl('https://hypnobox.dsinove.com/assets/images/red.png'));

            $this->send()->message($this->senderId, $message);
        }
    }

    /**
     * Envia mensagem simples para o usuario do facebook
     * @throws Exception
     */
    public function sendMessage()
    {
        if ($this->senderId === NULL) {
            throw new Exception("Error Facebook SenderId");
        }

        $this->send()->action($this->senderId, Send::SENDER_ACTION_MARK_SEEN);
        $this->send()->action($this->senderId, Send::SENDER_ACTION_TYPING_ON);

        $lastText = NULL;

        if (!$this->clear) array_push($this->reply, "_clear_");

        if ($this->reply && count($this->reply) > 0 && $this->text && count($this->text)) {
            $lastTextKey = count($this->text) - 1;
            $lastText = $this->text[$lastTextKey];

            unset($this->text[$lastTextKey]);
        }

        if ($this->text && count($this->text)) {
            foreach ($this->text as $text) {
                $this->send()->message($this->senderId, $text);
            }
        }

        $this->sendReply($lastText);

        $this->send()->action($this->senderId, Send::SENDER_ACTION_TYPING_OFF);
        $this->reset();
    }

    /**
     * Faz dados de algumas variaveis ficarem como defaul
     * @return void;
     */
    public function reset()
    {
        $this->senderId = NULL;
        $this->text = [];
        $this->reply = [];
        $this->audio = NULL;
    }
}
