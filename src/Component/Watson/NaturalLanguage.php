<?php
// src/Component/Watson/NaturalLanguage.php
namespace App\Component\Watson;

use Exception;
use Curl\Curl;


/**
 * Class NaturalLanguage
 * @package App\Component\Watson
 */
class NaturalLanguage
{
    /**
     * Endpoint do Serviço Watson Natural Language
     * @var string
     */
    private $url = "https://gateway.watsonplatform.net/natural-language-understanding/api/v1/analyze?version=2017-02-27";

    /**
     * Username do Serviço Watson Natural Language
     * @var null|string
     */
    private $username = NULL;

    /**
     * Password do Serviço Watson Natural Language
     * @var null|string
     */
    private $password = NULL;

    /**
     * NaturalLanguage constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->username = getenv('WATSON_NL_USERNAME');
        $this->password = getenv('WATSON_NL_PASSWORD');

        if (!isset($this->username) || !$this->username || !isset($this->password) || !$this->password) {
            throw new Exception("Error Watson Natural Language Env");
        }
    }

    /**
     * @param string $text
     * @return object
     * @throws Exception
     * @throws \ErrorException
     */
    public function analyze(string $text)
    {
        $data = $this->postNaturalLanguage($text);

        return $data;
    }

    /**
     * Faz a consulta na API de Linguagem Natural via Curl
     * @param object $dataCurl
     * @return object
     * @throws Exception
     * @throws \ErrorException
     */
    private function postNaturalLanguage($text)
    {
        $dataPost = (object)[
            'text' => $text,
            'language' => 'pt',
            'features' => (object)[
                'entities' => (object)[
                    'sentiment' => TRUE,
                    'limit' => 1
                ]
            ]
        ];

        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Cache-Control', 'no-cache');
        $curl->setOpt(CURLOPT_USERPWD, $this->username . ":" . $this->password);
        $curl->setOpt(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $curl->setOpt(CURLOPT_SSL_VERIFYHOST, 0);
        $curl->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);
        $curl->setOpt(CURLOPT_RETURNTRANSFER, TRUE);
        $curl->setOpt(CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        $curl->post($this->url, json_encode($dataPost));

        if ($curl->error) {
            throw new Exception("Error Watson: " . $curl->error_message . " - Response: " . $curl->response);
        }

        return json_decode($curl->response);
    }
}
