<?php
// src/Component/Watson/Conversation.php
namespace App\Component\Watson;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Exception;
use Curl\Curl;


/**
 * Class Conversation
 * @package App\Component\Watson
 */
class Conversation
{
    /**
     * @var Session
     */
    private $session;

    /**
     * Endpoint do Serviço Watson Conversation
     * @var string
     */
    private $url = "https://gateway.watsonplatform.net/conversation/api/v1/workspaces/{{workspace}}/message?version=2018-02-16";

    /**
     * Username do Serviço Watson Conversation
     * @var null|string
     */
    private $username = NULL;

    /**
     * Password do Serviço Watson Conversation
     * @var null|string
     */
    private $password = NULL;

    /**
     * Workspece do Serviço Watson Conversation
     * @var null|string
     */
    private $workspace = NULL;

    /**
     * Objeto que armazena o contexto
     * @var object
     */
    public $context;

    /**
     * Tag que será procurada na resposta do do Watson para finalizar uma conversa
     * @var string
     */
    public $tagClearContext = "sair";

    /**
     * Objeto que armazena tags encontradas na ultima resposta
     * @var null|object
     */
    public $textTags;

    /**
     * Array que armazena os textos da ultima resposta
     * @var array
     */
    public $text = [];

    /**
     * @var bool
     */
    public $clear = FALSE;

    /**
     * Conversation constructor.
     * @throws Exception
     */
    public function __construct($context = [])
    {
        $request = Request::createFromGlobals();

        $this->session = new Session();

        $this->username = getenv('WATSON_USERNAME');
        $this->password = getenv('WATSON_PASSWORD');
        $this->workspace = getenv('WATSON_WORKSPACE');

        if (!isset($this->username) || !$this->username
            || !isset($this->password) || !$this->password
            || !isset($this->workspace) || !$this->workspace) {
            throw new Exception("Error Watson Env");
        }

        $this->url = str_replace("{{workspace}}", $this->workspace, $this->url);

        $this->context = (object)[];
        $this->textTags = (object)[];

        if ($this->session->get('context')) {
            $this->setContext($this->session->get('context'));
        } else {
            $this->setContext($context);
        }
    }

    /**
     * Grava o 'context' para consultas posteriores
     * @param mixed $context
     * @param null|string $value
     * @return object
     */
    public function setContext($context = NULL, $value = NULL)
    {
        if (isset($context)) {
            if (is_array($context) || is_object($context)) {
                foreach ($context as $key => $value) {
                    $this->setContext($key, $value);
                }
            } elseif (isset($context) && $context && isset($value) && $value) {
                $this->context->{$context} = $value;
            }

            $this->session->set('context', $this->context);
        }

        return $this->context;
    }

    /**
     * Inicial o envio de uma pergunta ao Watson
     * @param string $text
     * @return object
     * @throws Exception
     * @throws \ErrorException
     */
    public function talk(string $text)
    {
        $dataPost = (object)[
            'context' => $this->context,
            'input' => (object)[
                'text' => $text
            ]
        ];

        $getFunc = $this->session->get('getFunc');

        if (isset($getFunc) && count($getFunc) > 0) {
            foreach ($getFunc as $row) {
                if (method_exists($this, $row)) {
                    $this->{$row}($text);
                }
            }
        }

        $dataWatson = $this->postConversation($dataPost);

        $dataParsed = $this->parse($dataWatson);

        return $dataParsed;
    }

    /**
     * Procura determinadas tags na resposta do Watson
     * @param array $text
     * @param bool $type
     * @return array
     */
    private function textTagsTags(array $text = [], $type = FALSE)
    {
        $get_vars = $type === FALSE;

        if ($get_vars) {
            $type = '.*.';
        }

        $pattern = "'\[" . $type . "\](.*?)\[\/" . $type . "\]'si";

        foreach ($text as $k => $t) {

            preg_match_all($pattern, $t, $tag);

            if (count($tag) > 0) {
                $t = preg_replace($pattern, '', $t);

                if (in_array($type, ['resposta', 'reply'])) {
                    foreach (end($tag) as $row) {
                        $row = str_replace(['; ', ' ;', ' ; '], ';', $row);

                        foreach (explode(';', $row) as $item) {
                            $this->textTags->reply[$item] = $item;
                        }
                    }
                } else {
                    foreach (reset($tag) as $row) {
                        preg_match("'\[(.*?)\]'si", $row, $tag_name);
                        if (count($tag_name) > 0) {
                            $this->textTags->{end($tag_name)} = str_replace(['[' . end($tag_name) . ']', '[/' . end($tag_name) . ']'], '', $row);
                        }
                    }
                }
            }

            $text[$k] = $t;
        }

        $this->text = $text;

        return $text;
    }

    /**
     * Trata os dados recebidos do Watson
     * @param object $data
     * @return object
     * @throws Exception
     * @throws \ErrorException
     */
    private function parse($data)
    {
        $this->setContext($data->context);

        $this->textTagsTags($data->output->text, 'reply');
        $this->textTagsTags($this->text);

        if (isset($this->textTags)) {
            if (isset($this->textTags->resumo)) {
                $this->resumo();
            }
        }

        $text2Speech = new Text2Speech();
        $audio = $text2Speech->createAudio($this->text, isset($this->textTags->reply) ? $this->textTags->reply : []);

        $this->getTextTagsFunc();

        $dataWatson = (object)[
            'text' => $this->text,
            'audio' => $audio
        ];

        if (isset($this->textTags)) {
            if (isset($this->textTags->reply) && count($this->textTags->reply)) {
                $dataWatson->reply = $this->textTags->reply;
            }

            if (isset($this->textTags->{$this->tagClearContext})) {
                $this->clearContext();
            }

            if (isset($this->textTags->resumo)) {
                $this->resumo();
            }
        }

        return $dataWatson;
    }

    /**
     * Faz a consulta no Watson Convesation via Curl enviado a pengunta
     * @param object $dataPost
     * @return object
     * @throws Exception
     * @throws \ErrorException
     */
    private function postConversation($dataPost)
    {
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Cache-Control', 'no-cache');
        $curl->setOpt(CURLOPT_USERPWD, $this->username . ":" . $this->password);
        $curl->setOpt(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $curl->setOpt(CURLOPT_SSL_VERIFYHOST, 0);
        $curl->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);
        $curl->setOpt(CURLOPT_RETURNTRANSFER, TRUE);
        $curl->setOpt(CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        $curl->post($this->url, json_encode($dataPost));

        if ($curl->error) {
            throw new Exception("Error Watson: " . $curl->error_message . " - Response: " . $curl->response);
        }

        return json_decode($curl->response);
    }

    /**
     * Limpa a sessão e o 'context'
     * Serve para finalizar uma conversa e iniciar uma nova
     * @return void
     */
    public function clearContext()
    {
        $this->clear = TRUE;
        $this->context = new \stdClass();
        $this->session->clear();
    }

    /**
     * Armazena em sessão as tags que contém getFunc
     * @return array
     */
    private function getTextTagsFunc()
    {
        $getFunc = [];

        if (isset($this->textTags) && count($this->textTags) > 0) {
            foreach ($this->textTags as $key => $row) {
                if (strpos($key, 'getFunc') !== FALSE) {
                    $getFunc[$row] = $row;
                }
            }
        }

        $this->session->set('getFunc', $getFunc);

        return $getFunc;
    }

    /**
     * Remove da sessão uma tag getFunc
     * @param string $name
     * @return void
     */
    private function removeTextTagsFunc(string $name)
    {
        $getFunc = $this->session->get('getFunc');
        unset($getFunc[$name]);
        $this->session->set('getFunc', $getFunc);
    }

    /**
     * Grava no context o nome através de analize
     * de linguagem natural
     * @param string $text
     * @throws Exception
     * @throws \ErrorException
     */
    private function getNome(string $text)
    {
        $nl = new NaturalLanguage();

        $data = $nl->analyze($text);

        $person = "";

        if (isset($data->entities) && count($data->entities)) {
            foreach ($data->entities as $row) {
                if ($row->type == "Person") {
                    $person = $row->text;
                }
            }
        }

        $this->setContext('nome', $person);

        $this->removeTextTagsFunc("getNome");
    }

    /**
     * Grava como nome o dado informado pelo cliente
     * @param string $text
     * @return void
     */
    private function getForceNome(string $text)
    {
        $this->setContext('nome', $text);

        $this->removeTextTagsFunc("getForceNome");
    }

    /**
     * Grava questões relevantes para o cliente
     * @param string $text
     * @return void
     */
    private function getCompraDetalheImportanteTexto(string $text)
    {
        $this->setContext('compraDetalheImportanteTexto', $text);

        $this->removeTextTagsFunc("getCompraDetalheImportanteTexto");
    }

    /**
     * Grava questões relevantes para o cliente
     * @param string $text
     * @return void
     */
    private function getCompraQuestoesRelevantes(string $text)
    {
        $this->setContext('compraQuestoesRelevantes', $text);

        $this->removeTextTagsFunc("getCompraQuestoesRelevantes");
    }

    /**
     * Gera um resumo da conversa
     * @return void
     */
    private function resumo()
    {
        $context = $this->context;

        $negocio = "Comprar um";

        /*if (in_array($context->negocio, ['alugar'])) {
            $negocio = "Alugar um";
        }*/

        $compraFase = "na planta";

        if (in_array(@$context->compraFase, ['em construção', 'construção', 'contrucao'])) {
            $compraFase = "em construção";
        } else if (in_array(@$context->compraFase, ['pronto', 'pronto para morar'])) {
            $compraFase = "pronto para morar";
        }

        $lines = [
            "Aqui vai um resumo:",
            "Nome: " . @$context->nome . ".",
            "Objetivo: " . $negocio . " imóvel " . $compraFase . ".",
            "Dormitórios: " . @$context->dormitorios . ".",
            "Tempo que procura um imóvel: " . @$context->compraTempoBusca . ".",
            "Detalhe Importante: " . @$context->compraDetalheImportante . " - \"" . @$context->compraDetalheImportanteTexto . "\".",
            "Questões relevantes: \"" . @$context->compraQuestoesRelevantes . "\".",
            "Decisão de compra: " . @$context->compraDecisao . ".",
            "Preferência de contato: " . @$context->contatoTipo . (@$context->contatoTipo != "E-mail" ? " - " . @$context->contatoPeriodo : "") . ".",

        ];

        foreach ($lines as $row) {
            array_push($this->text, $row);
        }

        $this->clearContext();
    }
}
