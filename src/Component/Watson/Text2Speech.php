<?php
// src/Component/Watson/Text2Speech.php
namespace App\Component\Watson;

use Exception;
use Curl\Curl;


/**
 * Class Text2Speech
 * @package App\Component\Watson
 */
class Text2Speech
{
    /**
     * Endpoint do Serviço Watson Speech
     * @var string
     */
    private $url = "https://stream.watsonplatform.net/text-to-speech/api/v1/synthesize";

    /**
     * Username do Serviço Watson Speech
     * @var null|string
     */
    private $username = NULL;

    /**
     * Password do Serviço Watson Speech
     * @var null|string
     */
    private $password = NULL;

    /**
     * Armarzena url do audio criado
     * @var null|string
     */
    public $audio = NULL;

    /**
     * Local onde será criado os audios
     * @var string
     */
    private $audioPath = "assets/upload/";

    /**
     * Text2Speech constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->username = getenv('WATSON_SPEECH_USERNAME');
        $this->password = getenv('WATSON_SPEECH_PASSWORD');

        if (!isset($this->username) || !$this->username || !isset($this->password) || !$this->password) {
            throw new Exception("Error Watson Speech Env");
        }
    }

    /**
     * Caso tenha o acesso ao serviço de de Speech do Watson,
     * Verifica ou cria um novo audio com base na pergunta
     * @param array $text
     * @param array $reply
     * @return null|string
     * @throws Exception
     * @throws \ErrorException
     */
    public function createAudio(array $text, array $reply = [])
    {
        $hostName = $_SERVER['HTTP_HOST'];
        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https://' ? 'https://' : 'http://';

        $text = implode(' ', $text);

        if (count($reply) > 0) {
            $text = $text . ' ' . implode(', ', $reply) . '.';
        }

        $fileName = md5($text);

        $file = $this->audioPath . $fileName . '.mp3';

        if (file_exists('./' . $file)) {
            $this->audio = $protocol . $hostName . "/" . $file;

        } else {
            $this->audio = $protocol . $hostName . "/" . $this->getAudio($file, $text);
        }

        return $this->audio;
    }

    /**
     * Consulta via Curl ao serviço de Speech para criar um audio
     * @param string $file
     * @param string $text
     * @return string
     * @throws Exception
     * @throws \ErrorException
     */
    private function getAudio($file, $text)
    {
        $f = fopen('./' . $file, 'w');

        $curl = new Curl();

        $curl->setHeader('Cache-Control', 'no-cache');
        $curl->setOpt(CURLOPT_RETURNTRANSFER, 1);
        $curl->setOpt(CURLOPT_USERPWD, $this->username . ":" . $this->password);
        $curl->setOpt(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $curl->setOpt(CURLOPT_SSL_VERIFYHOST, 0);
        $curl->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);
        $curl->setOpt(CURLOPT_RETURNTRANSFER, TRUE);
        $curl->setOpt(CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        $curl->setOpt(CURLOPT_FILE, $f);
        $curl->get($this->url, [
            'accept' => 'audio/mp3',
            'voice' => 'pt-BR_IsabelaVoice',
            'text' => $text
        ]);

        if ($curl->error) {
            throw new Exception("Error Watson: " . $curl->error_message . " - Response: " . $curl->response);
        }

        fclose($f);

        return $file;
    }
}
