<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title><?= $title; ?></title>
    <link rel="stylesheet" href="<?= $view['assets']->getUrl('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= $view['assets']->getUrl('assets/css/bootstrap-extend.min.css'); ?>">
    <link rel="stylesheet" href="<?= $view['assets']->getUrl('assets/css/site.min.css'); ?>">
    <link rel="stylesheet" href="<?= $view['assets']->getUrl('assets/material-design/material-design.min.css'); ?>">
    <link rel="stylesheet" href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link rel="stylesheet" href="<?= $view['assets']->getUrl('assets/css/owl.carousel.min.css'); ?>"/>
    <link rel="stylesheet" href="<?= $view['assets']->getUrl('assets/css/main.css'); ?>"/>
</head>
<body class="init">
<div class="panel" id="chat-conv">
    <div class="panel-heading">
        <div class="dark alert-icon alert alert-info"><i class="icon md-comment-list" aria-hidden="true"></i>
            Chatbot
        </div>
        <a class="volume-audio">
            <i class="icon md-volume-up" aria-hidden="true"></i>
            <i class="icon md-volume-off" aria-hidden="true"></i>
        </a>
    </div>
    <div class="panel-body">
        <form id="contact">
            <h4 class="pt-25 text-center">Para começarmos, informe seus dados.</h4>
            <div class="input-group form-material">
                <input class="form-control" type="text" name="nome" autofocus placeholder="Nome" required>
            </div>
            <div class="input-group form-material">
                <input class="form-control" type="email" name="email" placeholder="Email" required>
            </div>
            <div class="input-group form-material">
                <input class="form-control" type="text" name="telefone" placeholder="Telefone" required>
            </div>
            <button type="submit" style="float: right;"
                    class="btn btn-pure btn-default icon md-mail-send font-size-30"></button>
        </form>

        <div class="chat-box">
            <div class="chats">
                <div class="chat" v-bind:class="{ 'chat-left': chat.me, 'loading': chat.loading }"
                     v-for="chat in chats">
                    <div class="chat-body">
                        <div class="chat-content">
                            <span class="loading-text"><b></b></span>
                            <p><b>{{ chat.name }} diz:</b></p>
                            <span class="block" v-for="tx in chat.text">{{ tx }}<span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="reply-block" v-if="reply" v-bind:class="{ 'show': reply}">
            <div class="reply-container">
                <a onclick="sendMessageBtn(this);return false;" href="#" v-for="value in reply"
                   class="item btn btn-round btn-info btn-sm waves-effect waves-classic">{{ value }}</a>
            </div>
        </div>
    </div>
    <div class="panel-footer pb-30">
        <form id="chat">
            <div class="input-group form-material">
                <input class="form-control" type="text" placeholder="Digite aqui" required>
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-pure btn-default icon md-mail-send"></button>
                </span>
            </div>
        </form>
    </div>
</div>

<script src="<?= $view['assets']->getUrl('assets/js/jquery.min.js') ?>"></script>
<script src="<?= $view['assets']->getUrl('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?= $view['assets']->getUrl('assets/js/vue.min.js') ?>"></script>
<script src="<?= $view['assets']->getUrl('assets/js/jquery.mask.min.js') ?>"></script>
<script src="<?= $view['assets']->getUrl('assets/js/owl.carousel.min.js') ?>"></script>
<script src="<?= $view['assets']->getUrl('assets/js/main.js') ?>"></script>
</body>
</html>
