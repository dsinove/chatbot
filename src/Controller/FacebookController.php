<?php
// src/Controller/ChatController.php
namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Psr\Log\LoggerInterface;
use Exception;
use App\Component\Facebook\MessengerApi;
use Curl\Curl;

class FacebookController extends Controller
{
    /**
     * @var MessengerApi
     */
    private $messenger;

    /**
     * FacebookController constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $appSecret = getenv('FACEBOOK_APPSECRET');
        $verifyToken = getenv('FACEBOOK_VERIFYTOKEN');
        $pageToken = getenv('FACEBOOK_PAGETOKEN');

        $this->messenger = new MessengerApi($appSecret, $verifyToken, $pageToken);

    }


    /**
     * @param LoggerInterface $logger
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws Exception
     * @Route("/facebook/webhook")
     * @Method({"POST"})
     */
    public function webhook(LoggerInterface $logger)
    {
        $messages = $this->messenger->getMessages();

        foreach ($messages as $row) {

            $logger->debug(print_r(isset($row->quickReply) ? $row->quickReply : "", true));

            $curl = new Curl();
            $curl->setHeader('Cache-Control', 'no-cache');
            $curl->setOpt(CURLOPT_COOKIEJAR, '/tmp/cookies.txt');
            $curl->setOpt(CURLOPT_COOKIEFILE, '/tmp/cookies.txt');
            $curl->setOpt(CURLOPT_COOKIE, session_name() . "=" . md5($row->senderId) . ";");
            $curl->setOpt(CURLOPT_SSL_VERIFYHOST, 0);
            $curl->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);
            $curl->setOpt(CURLOPT_RETURNTRANSFER, TRUE);
            $curl->setOpt(CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            $curl->post("https://hypnobox.dsinove.com/" . (isset($row->quickReply) ? "?quickReply=" . $row->quickReply : ""), [
                'text' => $row->text
            ]);

            if ($curl->error) {
                throw new Exception("Error Watson: " . $curl->error_message . " - Response: " . $curl->response);
            }

            $data = json_decode($curl->response);

            $this->messenger->clear = $data->clear;

            if (isset($data->reply)) {
                $this->messenger->setReply((array)$data->reply);
            }

            if (isset($data->audio)) {
                $this->messenger->setAudio($data->audio);
            }

            $this->messenger->setSenderId($row->senderId)
                ->setText((array)$data->text);

            $this->messenger->sendMessage();
        }

        return $this->json("Message received");
    }


    /**
     * @Route("/facebook/webhook")
     * @Method({"GET"})
     * @return Response
     */
    public function subscribe()
    {
        if ($this->messenger->webhook()->isValidToken()) {
            $challenge = $this->messenger->webhook()->challenge();

            return new Response(
                $challenge
            );
        }

        return new Response("Error");

    }
}
