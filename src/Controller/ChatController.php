<?php
// src/Controller/ChatController.php
namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Component\Watson\Conversation;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;

class ChatController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET"})
     * @return Response
     */
    public function index()
    {
        return $this->render('chat/index.html.php', [
            'title' => "Chatbot"
        ]);
    }

    /**
     * @Route("/")
     * @Method({"POST"})
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function conversation(LoggerInterface $logger)
    {
        $request = Request::createFromGlobals();

        $context = (object)[];

        $text = $request->request->get('text');

        $init = $request->request->get('init');

        $watson = new Conversation();

        $context->timezone = "America/Sao_Paulo";

        if ($init){
            $watson->clearContext();

            $watson->talk('oi');

            $text = $text['nome'] . ', ' . $text['email'] . ', ' . $text['telefone'];
        }

        if ($request->query->get('quickReply') == "CLEAR"){
            $watson->clearContext();
            $text = 'sair';
        }

        $watson->setContext($context);

        $data = $watson->talk($text);

        $data->text = array_diff($data->text, ['', NULL]);

        $data->name = "Consultor";

        $data->context = $watson->context;

        $data->clear = $watson->clear;

        return $this->json($data);
    }
}
